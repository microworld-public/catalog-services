package com.sechambadi.catalog.price.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.math.BigDecimal;

@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Price implements Serializable {

  @PrimaryKey
  private String id;
  private BigDecimal price;

}
