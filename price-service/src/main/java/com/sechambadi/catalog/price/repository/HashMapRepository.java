package com.sechambadi.catalog.price.repository;


import com.sechambadi.catalog.price.model.Price;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class HashMapRepository {

  private Map<String, Price> priceMap = new HashMap<>();

  public Price save(Price price) {
    priceMap.put(price.getId(), price);
    return price;
  }

  public Optional<Price> findById(String id) {
    return Optional.of(priceMap.get(id));
  }


  public void deleteById(String id) {
    priceMap.remove(id);
  }

  public List<Price> findAll() {
    return new ArrayList<>(priceMap.values());
  }

  public List<Price> findAllById(Iterable<String> ids) {
    List<Price> matchingEntries = new ArrayList<>();
    ids.forEach(id -> matchingEntries.add(priceMap.get(id)));
    return matchingEntries;
  }

  public long saveAll(List<Price> priceList) {
    priceMap.clear();
    return priceList.stream().map(price -> priceMap.put(price.getId(), price)).count();
  }
}
