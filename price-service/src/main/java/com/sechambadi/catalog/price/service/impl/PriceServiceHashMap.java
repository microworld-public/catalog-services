package com.sechambadi.catalog.price.service.impl;

import com.sechambadi.catalog.price.model.Price;
import com.sechambadi.catalog.price.util.PriceUtils;
import com.sechambadi.catalog.price.repository.HashMapRepository;
import com.sechambadi.catalog.price.service.PriceService;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.PostConstruct;

@Service("priceService")
@Slf4j
@ConditionalOnProperty(name = "cassandra.enabled", havingValue = "false")
public class PriceServiceHashMap implements PriceService {

  @Autowired
  HashMapRepository repository;

  @Override
  public Price save(Price price) {
    log.info("saving price {}", price);
    return repository.save(price);
  }

  @Override
  public Price findById(Integer id) {
    log.info("finding price with id={}", id);
    return repository.findById(String.valueOf(id)).orElse(null);
  }

  @Override
  public Price update(Price price) {
    return save(price);
  }

  @Override
  public void delete(Integer id) {
    repository.deleteById(String.valueOf(id));
  }

  @Override
  public List<Price> findAll() {
    return repository.findAll();
  }

  @Override
  @Timed
  public List<Price> randomList(int qty) {
    log.info("id size {}", qty);
    Iterable<String> ids = PriceUtils.getRandomNumbers(qty);
    log.info("get for ids {}", ids);
    return repository.findAllById(ids);
  }

  @Override
  public Long load() {
    return repository.saveAll(PriceUtils.getRandomPrice());
  }

  @PostConstruct
  public void logRepositorySelection() {
    log.info("using a simple HashMap as the repository");
  }


}
