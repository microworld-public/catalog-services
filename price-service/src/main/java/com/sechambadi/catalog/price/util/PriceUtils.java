package com.sechambadi.catalog.price.util;

import com.sechambadi.catalog.price.model.Price;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PriceUtils {

  public static final int CAPACITY = 10000;

  public static List<Price> getRandomPrice() {
    List<Price> priceList = new ArrayList<>(CAPACITY);
    for (int i = 0; i < CAPACITY; i++) {
      Price price = new Price();
      price.setId(String.valueOf(i + 1));
      price.setPrice(new BigDecimal(getRandomNumber()));
      priceList.add(price);
    }
    return priceList;
  }

  public static Iterable<String> getRandomNumbers(final int qty) {
    List<String> stringList = new ArrayList<>(qty);
    Arrays.stream(getRandomArray(qty, 1, CAPACITY))
        .forEach(m -> stringList.add(Integer.toString(m)));
    return stringList;
  }

  private static int getRandomNumber() {
    return getRandomArray(1,1,100)[0];
  }

  private static int[] getRandomArray(int qty, int i, int capacity) {
    return new Random()
        .ints(qty, i, capacity)
        .toArray();
  }

}
