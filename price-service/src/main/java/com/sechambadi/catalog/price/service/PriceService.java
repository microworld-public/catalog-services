package com.sechambadi.catalog.price.service;


import com.sechambadi.catalog.price.model.Price;

import java.util.List;

public interface PriceService {

  Price save(Price price);

  Price findById(Integer id);

  Price update(Price price);

  void delete(Integer id);

  List<Price> findAll();

  List<Price> randomList(int qty);

  Long load();
}
