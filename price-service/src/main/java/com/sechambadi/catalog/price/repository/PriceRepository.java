package com.sechambadi.catalog.price.repository;

import com.sechambadi.catalog.price.model.Price;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface PriceRepository extends CassandraRepository<Price, String> {

}
