package com.sechambadi.catalog.aggregate.util;

import com.sechambadi.catalog.aggregate.model.Price;
import com.sechambadi.catalog.aggregate.model.Product;
import com.sechambadi.catalog.aggregate.model.ProductPrice;

public class AggregateUtils {
  public static ProductPrice getFromComponents(Product product, Price price) {
    if (!product.getId().equalsIgnoreCase(price.getId())) {
      throw new IllegalArgumentException("ids do not match" + product + price);
    }
    ProductPrice productPrice = new ProductPrice();
    productPrice.setId(price.getId());
    productPrice.setDescription(product.getDescription());
    productPrice.setPrice(price.getPrice());
    return productPrice;
  }

}
