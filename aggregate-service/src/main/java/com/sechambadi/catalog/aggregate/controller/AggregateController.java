package com.sechambadi.catalog.aggregate.controller;

import com.sechambadi.catalog.aggregate.client.PriceClient;
import com.sechambadi.catalog.aggregate.client.ProductClient;
import com.sechambadi.catalog.aggregate.model.Price;
import com.sechambadi.catalog.aggregate.model.Product;
import com.sechambadi.catalog.aggregate.model.ProductPrice;
import com.sechambadi.catalog.aggregate.util.AggregateUtils;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
@RequestMapping("/aggregate")
public class AggregateController {

  @Autowired
  ProductClient productClient;

  @Autowired
  PriceClient priceClient;

  @Autowired
  MeterRegistry registry;

  @GetMapping("/{productId}")
  public ProductPrice getProduct(@PathVariable String productId) {
    CompletableFuture<Product> productFuture = CompletableFuture.supplyAsync(() -> productClient.findById(productId));
    CompletableFuture<Price> priceFuture = CompletableFuture.supplyAsync(() -> priceClient.findById(productId));

    CompletableFuture result = CompletableFuture.allOf(priceFuture, productFuture);
    ProductPrice productPrice = new ProductPrice();
    productPrice.setId(productId);
    try {
      result.get();
      Price price = priceFuture.get();
      Product product = productFuture.get();
      productPrice = AggregateUtils.getFromComponents(product, price);
    } catch (InterruptedException e) {
      log.error("exception getting resutl", e);
    } catch (ExecutionException e) {
      log.error("exception getting resutl", e);
    }
    return productPrice;
  }

  @GetMapping("/api/stats")
  public Map<String, Number> stats() {
    return Optional.ofNullable(registry.find("http.server.requests").tags("uri", "/product/random/{qty}")
        .timer())
        .map(t -> new HashMap<String, Number>() {{
          put("count", t.count());
          put("max", t.max(TimeUnit.MILLISECONDS));
          put("mean", t.mean(TimeUnit.MILLISECONDS));
          put("50.percentile", t.percentile(0.5, TimeUnit.MILLISECONDS));
          put("90.percentile", t.percentile(0.90, TimeUnit.MILLISECONDS));
          put("999.percentile", t.percentile(0.999, TimeUnit.MILLISECONDS));
          put("95.percentile", t.percentile(0.95, TimeUnit.MILLISECONDS));
        }})
        .orElse(null);
  }

}
