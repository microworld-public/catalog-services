

package com.sechambadi.catalog.aggregate.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

  private String id;

  private String description;

}
