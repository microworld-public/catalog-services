package com.sechambadi.catalog.aggregate.client;

import com.sechambadi.catalog.aggregate.model.Price;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "price", url = "http://localhost:9772") for local testing
@FeignClient(name = "price-service")
public interface PriceClient {
  @GetMapping("/price/{productId}")
  Price findById(@PathVariable("productId") String productId);
}
