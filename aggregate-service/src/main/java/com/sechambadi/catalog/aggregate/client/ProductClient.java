package com.sechambadi.catalog.aggregate.client;

import com.sechambadi.catalog.aggregate.model.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "product", url = "http://localhost:9770") for local testing
@FeignClient(name = "product-service")
public interface ProductClient {
  @GetMapping("/product/{productId}")
  Product findById(@PathVariable("productId") String productId);
}
