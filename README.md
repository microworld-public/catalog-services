# Getting Started

### Guides
The following guides illustrates how to use certain features concretely:

* [Spring guide home](https://spring.io/guides)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Building a Spring Boot with gradle](https://spring.io/guides/gs/gradle/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [Spring kubernetes guide](https://www.baeldung.com/spring-cloud-kubernetes)

* Has a simple prometheus file to get the metrics data
* Grafana dashboard to display the metrics from the prometheus
* Micrometer to push the data
* lmax and log4j dependencies for async logger


Setup Cassandra
----------------
* cassandra dependency is optional included in the dependency and turned off using the flag cassandra.enabled in the config.
* Enble the flag in the config
* Also create the keyspace and the table using the  catalog-services/resources/cassandra/create.cql in the cassandra directory
* under resources.

Project Structure
-----------------

There are 2 spring boot projects and 1 common project

1. price service  that uses spring cassandra to fetch data
2. catalog-common that has the common files for both the projects

Build the project
------------------
* to build the project use the following
* cd <path-to-code>/catalog-services
* ./gradlew clean build

Run the project
---------------
java -jar  catalog-services/price-service/build/libs/price-service-0.0.1-SNAPSHOT.jar


Load Sample Data
-----------------
** to generate sample data for price-service use the following url in postman http://localhost:9260/price/load
** or use the curl curl -X POST http://localhost:9260/price/load


Build Docker Container
----------------------

The project is configured to use the jib instead of docker to test the build process of creating the container.
More details available at [jib quickstart](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin#quickstart)
./gradlew  jibBuildTar

Note: the normal docker plugin is also available in the build file but it is commented out 

Rest End points
---------------
* [get price based on id using http get](http://localhost:[9770]/price/{id})
* [save price using http post](http://localhost:[9770]/price/save)
* [update a price using http post](http://localhost:[9770]/price/update)
* [delete a price using http delete](http://localhost:[9770]/price/delete/{id})
* [get all price in the repository using http get](http://localhost:[9770]/findall)
* [get some random products in the repository using http get](http://localhost:[9770]/random/{qty})
* [load sample data http post](http://localhost:[9770]/load)

Cassandra Kubernates setup 
----------------------------
There are some sample files based on the following

* [Cassandra stateful setup in with volumes](https://github.com/IBM/Scalable-Cassandra-deployment-on-Kubernetes)
* [cassandra stateful setup](https://kubernetes.io/docs/tutorials/stateful-application/cassandra/)

Running Jmeter
--------------
* to run the jmeter use the following command for logback test which gets 5 random records
* ./jmeter -n -t <path-to-code>/ catalog-services/resources/jmeter/getRandom-logback.jmx

* to run the jmeter use the following command for log4j2 test which gets 5 random records
* ./jmeter -n -t <path-to-code>/ catalog-services/resources/jmeter/getRandom-log4j2.jmx

Prometheus Configuration
------------------------
Simply Replace the file  catalog-services/src/main/resources/prometheus/prometheus.yml as the
prometheus config in the prometheus and start it using. Home page (https://prometheus.io/)
1. cd <path>/prometheus-2.8.0
2. cp prometheus.yml prometheus.yml.original
3. cp <path>/ catalog-services/src/main/resources/prometheus/prometheus.yml .
4. ./prometheus

now open http://localhost:9092 to checkout prometheus

Monitoring Configuration
------------------------

To set up Grafana to visualize system health you should do
the following easy steps. Home page (https://grafana.com/)

default url for grafana http://localhost:3000

 1. Open Grafana UI
 2. Create Prometheus data source (click `Save & Test` when you've done):
    * Name: choose name you like
    * Type: `Prometheus`
    * URL: `http://local:9092`
    * Leave all security checkboxes empty
 3. Import dashboards:
    * Jvm  dashboard for Prometheus is available in the grafana directory
    * under  catalog-services/resources/grafana/jvm-micrometer_rev7.json
      1. Paste this URL to `Grafana.com Dashboard` field
      2. Select data source created in previous step
      3. Click `Import`
    * springboot statistics dashboard is available under
    *  catalog-services/resources/grafana/spring-boot-statistics_rev2.json
      1. Paste contents of this JSON to `Or paste JSON`
      2. Select data source created in previous step
      3. Click `Import`
    * simple dashboard with hardcoded values is available under
        *  catalog-services/resources/grafana/simple.json
          1. Paste contents of this JSON to `Or paste JSON`
          2. Select data source created in previous step
          3. Click `Import`
  4. In the dashboard there are two input boxes on the top left of the screen for springboot statistics dashboard
     for logback
     * type instance: localhost:9770
     * type application: price-service

