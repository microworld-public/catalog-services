package com.sechambadi.catalog.product.repository;

import com.sechambadi.catalog.product.model.Product;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface ProductRepository extends CassandraRepository<Product, String> {

}
