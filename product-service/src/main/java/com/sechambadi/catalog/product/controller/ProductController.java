package com.sechambadi.catalog.product.controller;

import com.sechambadi.catalog.product.model.Product;
import com.sechambadi.catalog.product.service.ProductService;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/product")
public class ProductController {

  @Autowired
  ProductService service;

  @Autowired
  MeterRegistry registry;

  @PostMapping("/save")
  Product save(@RequestBody Product product) {
    return service.save(product);
  }

  @GetMapping("/{id}")
  Product findById(@PathVariable("id") Integer id) {
    return service.findById(id);
  }

  @PostMapping("/update")
  Product update(@RequestBody Product product) {
    return service.update(product);
  }

  @DeleteMapping("/delete/{id}")
  void delete(@PathVariable("id") Integer id) {
    service.delete(id);
  }

  @GetMapping("/findall")
  List<Product> findAll() {
    return service.findAll();
  }

  @GetMapping("/random/{qty}")
  @Timed(percentiles = {0.5, 0.9, 0.95, 0.999}, histogram = true)
  List<Product> randomList(@PathVariable("qty") int qty) {
    return service.randomList(qty);
  }

  @PostMapping("/load")
  Long load() {
    return service.load();
  }

  @GetMapping("/api/stats")
  public Map<String, Number> stats() {
    return Optional.ofNullable(registry.find("http.server.requests").tags("uri", "/product/random/{qty}")
        .timer())
        .map(t -> new HashMap<String, Number>() {{
          put("count", t.count());
          put("max", t.max(TimeUnit.MILLISECONDS));
          put("mean", t.mean(TimeUnit.MILLISECONDS));
          put("50.percentile", t.percentile(0.5, TimeUnit.MILLISECONDS));
          put("90.percentile", t.percentile(0.90, TimeUnit.MILLISECONDS));
          put("999.percentile", t.percentile(0.999, TimeUnit.MILLISECONDS));
          put("95.percentile", t.percentile(0.95, TimeUnit.MILLISECONDS));
        }})
        .orElse(null);
  }

}
