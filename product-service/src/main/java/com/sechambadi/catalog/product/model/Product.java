package com.sechambadi.catalog.product.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

  @PrimaryKey
  private String id;

  private String description;

}
