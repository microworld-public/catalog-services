package com.sechambadi.catalog.product.service.impl;

import com.sechambadi.catalog.product.model.Product;
import com.sechambadi.catalog.product.repository.HashMapRepository;
import com.sechambadi.catalog.product.service.ProductService;
import com.sechambadi.catalog.product.util.ProductUtils;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.PostConstruct;

@Service("productService")
@Slf4j
@ConditionalOnProperty(name = "cassandra.enabled", havingValue = "false")
public class ProductServiceHashMap implements ProductService {

  @Autowired
  HashMapRepository repository;

  @Override
  public Product save(Product product) {
    log.info("saving product {}", product);
    return repository.save(product);
  }

  @Override
  public Product findById(Integer id) {
    log.info("finding product with id={}", id);
    return repository.findById(String.valueOf(id)).orElse(null);
  }

  @Override
  public Product update(Product product) {
    return save(product);
  }

  @Override
  public void delete(Integer id) {
    repository.deleteById(String.valueOf(id));
  }

  @Override
  public List<Product> findAll() {
    return repository.findAll();
  }

  @Override
  @Timed
  public List<Product> randomList(int qty) {
    log.info("id size {}", qty);
    Iterable<String> ids = ProductUtils.getRandomNumbers(qty);
    log.info("get for ids {}", ids);
    return repository.findAllById(ids);
  }

  @Override
  public Long load() {
    return repository.saveAll(ProductUtils.getRandomPeople());
  }

  @PostConstruct
  public void logRepositorySelection() {
    log.info("using a simple HashMap as the repository");
  }


}
