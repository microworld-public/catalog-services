package com.sechambadi.catalog.product.util;

import com.sechambadi.catalog.product.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ProductUtils {

  private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz";

  public static final int CAPACITY = 10000;

  public static String randomName() {
    StringBuilder builder = new StringBuilder();
    Arrays.stream(getRandomArray(10, 0, ALPHA_NUMERIC_STRING.length() - 1))
        .forEach(n -> builder.append(ALPHA_NUMERIC_STRING.charAt(n)));
    return builder.toString();
  }

  public static Iterable<String> getRandomNumbers(final int qty) {
    List<String> stringList = new ArrayList<>(qty);
    Arrays.stream(getRandomArray(qty, 1, CAPACITY))
        .forEach(m -> stringList.add(Integer.toString(m)));
    return stringList;
  }

  public static List<Product> getRandomPeople() {
    List<Product> productList = new ArrayList<>(CAPACITY);
    for (int i = 0; i < CAPACITY; i++) {
      Product product = new Product();
      product.setId(String.valueOf(i + 1));
      product.setDescription(randomName());
      productList.add(product);
    }
    return productList;
  }

  private static int[] getRandomArray(int qty, int i, int capacity) {
    return new Random()
        .ints(qty, i, capacity)
        .toArray();
  }

}
