package com.sechambadi.catalog.product.repository;


import com.sechambadi.catalog.product.model.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class HashMapRepository {

  private Map<String, Product> productMap = new HashMap<>();

  public Product save(Product product) {
    productMap.put(product.getId(), product);
    return product;
  }

  public Optional<Product> findById(String id) {
    return Optional.of(productMap.get(id));
  }


  public void deleteById(String id) {
    productMap.remove(id);
  }

  public List<Product> findAll() {
    return new ArrayList<>(productMap.values());
  }

  public List<Product> findAllById(Iterable<String> ids) {
    List<Product> matchingEntries = new ArrayList<>();
    ids.forEach(id -> matchingEntries.add(productMap.get(id)));
    return matchingEntries;
  }

  public long saveAll(List<Product> productList) {
    productMap.clear();
    return productList.stream().map(product -> productMap.put(product.getId(), product)).count();
  }
}
