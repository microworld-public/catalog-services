package com.sechambadi.catalog.product.service;


import com.sechambadi.catalog.product.model.Product;

import java.util.List;

public interface ProductService {

  Product save(Product product);

  Product findById(Integer id);

  Product update(Product product);

  void delete(Integer id);

  List<Product> findAll();

  List<Product> randomList(int qty);

  Long load();
}
