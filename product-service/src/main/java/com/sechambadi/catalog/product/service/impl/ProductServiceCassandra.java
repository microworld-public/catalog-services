package com.sechambadi.catalog.product.service.impl;

import com.sechambadi.catalog.product.model.Product;
import com.sechambadi.catalog.product.repository.ProductRepository;
import com.sechambadi.catalog.product.service.ProductService;
import com.sechambadi.catalog.product.util.ProductUtils;
import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.PostConstruct;

@Service("productService")
@Slf4j
@ConditionalOnProperty(name = "cassandra.enabled", havingValue = "true")
public class ProductServiceCassandra implements ProductService {

  @Autowired
  ProductRepository repository;

  @Override
  public Product save(Product product) {
    return repository.save(product);
  }

  @Override
  public Product findById(Integer id) {
    return repository.findById(String.valueOf(id)).orElse(null);
  }

  @Override
  public Product update(Product product) {
    return save(product);
  }

  @Override
  public void delete(Integer id) {
    repository.deleteById(String.valueOf(id));
  }

  @Override
  public List<Product> findAll() {
    return repository.findAll();
  }

  @Override
  @Timed
  public List<Product> randomList(int qty) {
    log.info("id size {}", qty);
    Iterable<String> ids = ProductUtils.getRandomNumbers(qty);
    log.info("get for ids {}", ids);
    return repository.findAllById(ids);
  }

  @Override
  public Long load() {
    return Long.valueOf(repository.saveAll(ProductUtils.getRandomPeople()).size());
  }

  @PostConstruct
  public void logRepositorySelection() {
    log.info("using a simple Cassandra as the repository");
  }

}
